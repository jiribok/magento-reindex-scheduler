<?php
/**
 * Created by PhpStorm.
 * User: jiri.bok
 * Date: 10/12/15
 * Time: 3:00 PM
 */

/**
 * This module is meant to be used with Aoe_Scheduler in tandem!
 */

class Lundegaard_ReindexScheduler_Model_Dispatcher extends Mage_Core_Model_Abstract {

    /**
     * Reindex all indexes programatically
     */
    public function runReindexAll(){
        /* ----- Available reindex codes -----
         * catalog_product_attribute     Product Attributes
         * catalog_product_price         Product Prices
         * catalog_url                   Catalog URL Rewrites
         * catalog_product_flat          Product Flat Data
         * catalog_category_flat         Category Flat Data
         * catalog_category_product      Category Products
         * catalogsearch_fulltext        Catalog Search Index
         * cataloginventory_stock        Stock Status
         * tag_summary                   Tag Aggregation Data
         * targetrule                    Target Rules
         */

        // Try to reindex everything
        $this->reindex('catalog_product_attribute', true);
        $this->reindex('catalog_product_price', true);
        $this->reindex('catalog_url', true);
        $this->reindex('catalog_product_flat', true);
        $this->reindex('catalog_category_flat', true);
        $this->reindex('catalog_category_product', true);
        $this->reindex('catalogsearch_fulltext', true);
        $this->reindex('cataloginventory_stock', true);
        $this->reindex('tag_summary', true);
        $this->reindex('targetrule', true);
    }

    #region Reindex indexes
    /**
     * Reindex Product Attributes
     */
    public function runReindexCatalogProductAttribute(){
        $this->reindex('catalog_product_attribute');
    }

    /**
     * Reindex Product Prices
     */
    public function runReindexCatalogProductPrice(){
        $this->reindex('catalog_product_price');
    }

    /**
     * Reindex Catalog URL Rewrites
     */
    public function runReindexCatalogUrl(){
        $this->reindex('catalog_url');
    }

    /**
     * Reindex Product Flat Data
     */
    public function runReindexCatalogProductFlat(){
        $this->reindex('catalog_product_flat');
    }

    /**
     * Reindex Category Flat Data
     */
    public function runReindexCatalogCategoryFlat(){
        $this->reindex('catalog_category_flat');
    }

    /**
     * Reindex Category Products
     */
    public function runReindexCatalogCategoryProduct(){
        $this->reindex('catalog_category_product');
    }

    /**
     * Reindex Catalog Search Index
     */
    public function runReindexCatalogSearchFulltext(){
        $this->reindex('catalogsearch_fulltext');
    }

    /**
     * Reindex Stock Status
     */
    public function runReindexCatalogInventoryStock(){
        $this->reindex('cataloginventory_stock');
    }

    /**
     * Reindex Tag Aggregation Data
     */
    public function runReindexTagSummary(){
        $this->reindex('tag_summary');
    }

    /**
     * Reindex Target Rules
     */
    public function runReindexTargetRule(){
        $this->reindex('targetrule');
    }
    #endregion

    #region Reindex indexes
    /**
     * Reindex Product Attributes
     */
    public function runReindexCatalogProductAttributeForce(){
        $this->reindex('catalog_product_attribute', true);
    }

    /**
     * Reindex Product Prices
     */
    public function runReindexCatalogProductPriceForce(){
        $this->reindex('catalog_product_price', true);
    }

    /**
     * Reindex Catalog URL Rewrites
     */
    public function runReindexCatalogUrlForce(){
        $this->reindex('catalog_url', true);
    }

    /**
     * Reindex Product Flat Data
     */
    public function runReindexCatalogProductFlatForce(){
        $this->reindex('catalog_product_flat', true);
    }

    /**
     * Reindex Category Flat Data
     */
    public function runReindexCatalogCategoryFlatForce(){
        $this->reindex('catalog_category_flat', true);
    }

    /**
     * Reindex Category Products
     */
    public function runReindexCatalogCategoryProductForce(){
        $this->reindex('catalog_category_product', true);
    }

    /**
     * Reindex Catalog Search Index
     */
    public function runReindexCatalogSearchFulltextForce(){
        $this->reindex('catalogsearch_fulltext', true);
    }

    /**
     * Reindex Stock Status
     */
    public function runReindexCatalogInventoryStockForce(){
        $this->reindex('cataloginventory_stock', true);
    }

    /**
     * Reindex Tag Aggregation Data
     */
    public function runReindexTagSummaryForce(){
        $this->reindex('tag_summary', true);
    }

    /**
     * Reindex Target Rules
     */
    public function runReindexTargetRuleForce(){
        $this->reindex('targetrule', true);
    }
    #endregion

    /**
     * This function is run by cron, and calls import on all storeviews
     *
     * @params $code process code
     * @params $overrideActive force execution
     */
    private function reindex($code = '', $overrideActive = false){
        if (!empty($code)) {
            $active = Mage::getStoreConfigFlag('reindexscheduler/settings_'.$code.'/active');
            if ($active || $overrideActive) {
                $process = Mage::getModel('index/indexer')->getProcessByCode($code);
                $process->reindexAll();
            }
        }
    }

} 